(function($, window, undefined) {

  function share_popin(url) {
    window.open(
      url,
      'share',
      'toolbar=0,status=0,width=626,height=500'
    );
  }

  $(document).ready(function() {
    $('.share-item').on('click', function(e) {
      e.stopPropagation();
      e.preventDefault();

      var $this = $(this),
      id = $this.attr('id'),
      dlObj = {
        category: "share",
        action: "click",
        event: "share",
        virtualPageURL:window.virtualPageURL,
        virtualPageTitle: window.virtualPageTitle,
        virtualHostName:window.virtualHostName
      };


      switch (id) {
        case 'share-facebook':
        dlObj.label = "facebook";
        share_popin($this.attr('href'));
        break;

        case 'share-twitter':
        dlObj.label = "twitter";
        share_popin($this.attr('href'));
        break;

        case 'share-email':
        dlObj.label = "email";
        break;
      }



      window.dataLayer.push(dlObj);

    })
  });
})(jQuery, window)
