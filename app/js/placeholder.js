(function($, undefined) {
  $(document).ready(function() {
		var todo = [];

		if (document.createElement("input").placeholder == undefined) {
			return;
		}
		
    $('#form-sticky').find('.eaFormField').each(function(){
			
    	var $t = $(this),
					$label = $t.parent().find('label'),
					text = $label.find('a').text(),
					mandatory = $label.find('.eaMandatoryFieldMarker'),
					$input = $t.find('input[type="text"]');

			todo.push({
				input: $input,
				placeholder: text + (mandatory.length == 1 ? ' *' : ''),
				label: $label
			});

			
    });

		for (var i = 0, l = todo.length; i < l; i++) {
			var t = todo[i];
			
      t.input.attr(
        'placeholder',
        t.placeholder
      );

			t.label.hide();
		}
  });

})(jQuery);
