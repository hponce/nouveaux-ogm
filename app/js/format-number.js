(function($, window, undefined) {

  $(window).on('counter.displayed', function() {
    $('.format-number').each(function() {
      var $this = $(this),
      numbers = $this.text().split('').reverse(),
      formatted = [];

      for (var i = 0, l = numbers.length; i < l; i++) {
        if (i % 3 === 0) {
          formatted.push(' ');
        }
        formatted.push(numbers[i]);


      }

      $this.text(formatted.reverse().join(''));

    });
  });

})(jQuery, window);
