(function(window, document, undefined) {
  window.onload = function() {
    var name = document.getElementById("first_name");
    if (name) {
      name.removeAttribute('onblur');
      document.getElementById("last_name").removeAttribute('onblur');
      document.getElementById("email").removeAttribute('onblur');
      document.getElementById("phone_number").removeAttribute('onblur');
    }
  }
})(window, window.document);
