(function($, window, undefined) {
  var $sticky,
  $first_field,
  $showForm,
  $body,
  $window = $(window),
  document = window.document,
  margin_top = 0,
  scrolled = 0;
  window_width = 0,
  mobile_breakpoint = 768;



  function stickForm() {
    var fixed = false;

    var form_top = $first_field.offset().top;

    if (window_width < mobile_breakpoint) {
       form_top = form_top + $sticky.height() - $first_field.height();
    }
    else {
      form_top = form_top - margin_top - 15;
    }


    if (scrolled > form_top) {
      fixed = true;
    }


    if (window_width >= mobile_breakpoint) {
      if (fixed) {
        margin_top = scrolled - form_top;
        $first_field.css('margin-top', margin_top );
      }
      else {
        $first_field.css('margin-top', 0 );
      }
    }
    else {
      if (fixed) {
        $body.addClass('fixed');
      }
      else {
        $body.removeClass('fixed');
      }
    }
  }

  function onResize() {
    window_width = $window.width();

    stickForm();
  }


  $window
  .on('scroll', function() {
    scrolled = $window.scrollTop();
    stickForm();
  })
  .on('resize', onResize);
  //.on('counter.displayed', onResize);

  $(document).ready(function() {
    //$sticky = $('#form-sticky');
    $sticky = $('#form-sticky');
    $first_field = $sticky.find('.eaFullWidthContent:first');
    $showForm = $('#show-form');
    $body = $('body');

    $showForm.on('click', function() {
      $('html,body').animate({scrollTop: $('#intro').offset().top}, scrolled * .5);
    });

    onResize();
  });


})(jQuery, window);
