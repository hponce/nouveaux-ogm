(function($, undefined) {
  var $more;

  $(document).ready(function() {
    $more = $('.more');

    $more.find('h3').on('click', function() {
      var $this = $(this),
      $moreContent = $this.parents('.more').find('.more-content');

      $moreContent.slideToggle();
    })
  });
})(jQuery);
