(function($, window, undefined) {
  $(window).on('petition.signed', function() {
    $('#intro').hide();
    $('#reassurance').hide();
  });
})(jQuery, window);
