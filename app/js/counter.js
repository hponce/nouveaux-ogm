(function($, window, undefined) {

	var urls = [
		'/ea-dataservice/data.service?service=EaDataCapture&token=fe6e9cce-2df8-4867-b0bb-af4e0fe9ceed&campaignId=49888&contentType=json&resultType=summary',
		'/ea-dataservice/data.service?service=EaDataCapture&token=cf9ed3f6-9aec-471d-b393-c617cd139dbd&campaignId=50199&contentType=json&resultType=summary',
		'/ea-dataservice/data.service?service=EaDataCapture&token=2a27df97-5649-4f82-b2f9-2d820837a1ca&campaignId=50212&contentType=json&resultType=summary',
	],
			counts = urls.length,
			total= 0,
  $counter,
  $progressBar,
  $nbSignatures,
  speed = 1000,
  easing = "linear";
	
  function formatNumber(n) {
    n = "" + n;
    numbers = n.split('').reverse(),
    formatted = [];

    for (var i = 0, l = numbers.length; i < l; i++) {
      if (i % 3 === 0) {
        formatted.push('&nbsp;');
      }
      formatted.push(numbers[i]);
    }

    return formatted.reverse().join('');

  }

  function onCounterLoaded(data) {
    if (data && data.rows) {
			counts = counts - 1;

			var r = data.rows[0].columns;

			for (var i = 0, l = r.length; i < l; i++) {
				var c = r[i];

				if (c.name && c.name === "participatingSupporters") {
					total = total + parseInt(c.value);
				}
			}
			
			if (counts <= 0) {
				var target = parseInt($progressBar.data('target')),
						percent = Math.floor(total / target * 100);

				$nbSignatures.html(formatNumber(total));

				$counter.addClass('show');

				$progressBar.animate({width: percent + "%"}, speed, easing, function() {
					$(window).trigger('counter.displayed');
				});

			}
			

    }
  }

	
  $(document).ready(function() {
    $counter = $('#counter');
    $progressBar = $counter.find('#progress-bar');
    $nbSignatures = $counter.find('#nb-signatures');

    $counter.find('.format-number').each(function() {
      var $this = $(this);

      $this.html( formatNumber($this.text()) );
    });
  });

  $(window).on('load', function() {

		for (var i = 0, l = urls.length; i < l; i++) {
			$.ajax({
				url: urls[i],
				dataType: "json",
				success: onCounterLoaded
			})
		}
  });

})(jQuery, window);
