'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Configurable paths

	var twigData = grunt.file.readJSON('app/twig.json'),
	config = grunt.file.readJSON('config.json');

	// Define the configuration for all the tasks
	grunt.initConfig({
		twigData: twigData,

		// Project settings
		config: config,

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'<%= config.dist %>/*',
						'<%= config.app %>/css/main.css',
						'!<%= config.dist %>/.git*'
					]
				}]
			}
		},


		// Twig
		twigRender: {
			options: {
			},
			dist: {
				options: {
				},

				files : [
					{
						//"data": "<%= config.app %>/data/main.json",
						"data": twigData,
						"expand": true,
						"cwd": "<%= config.app %>/views",
						"src": config.twigFiles,
						"dest": "<%= config.dist %>",
						"ext": ".html"
					}
				]
			},
			dev: {
				options: {
				},

				files : [
					{
						//"data": "<%= config.app %>/data/main.json",
						"data": twigData,
						"expand": true,
						"cwd": "<%= config.app %>/views",
						"src": ['test.twig'],
						"dest": "<%= config.dist %>",
						"ext": ".html"
					}
				]
			}
		},


		less: {
			prod: {
				options: {
					paths: ["<%= config.app %>/less"]
				},
				files:  [
					{
						expand: true,
						cwd: '<%= config.app %>/less',
						src: ['main.less'],
						dest: '<%= config.dist %>/',
						ext: '.css'
					}
				]
			}
		},

		concat: {
		    options: {
		      separator: ';',
		    },
		    dist: {
		      src: config.javascript,
		      dest: '<%= config.dist %>/main.js',
		    },
		  },

		// The following *-min tasks produce minified files in the dist folder
		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.dist %>/images',
					src: '{,*/}*.{gif,jpeg,jpg,png}',
					dest: '<%= config.dist %>/images'
				}]
			},
			beurk: {
				files: [{
					expand: true,
					cwd: '<%= config.app %>/images',
					src: 'bg.jpg',
					dest: '<%= config.app %>/images'
				}]
			}
		},

		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.dist %>/images',
					src: '{,*/}*.svg',
					dest: '<%= config.dist %>/images'
				}]
			}
		},

		htmlmin: {
			dist: {
				options: {
					collapseBooleanAttributes: true,
					collapseWhitespace: true,
					conservativeCollapse: true,
					removeAttributeQuotes: false,
					removeCommentsFromCDATA: false,
					removeEmptyAttributes: true,
					removeOptionalTags: true,
					removeRedundantAttributes: true,
					useShortDoctype: true
				},
				files: [{
					expand: true,
					cwd: '<%= config.dist %>',
					src: '**/*.html',
					dest: '<%= config.dist %>'
				}]
			}
		},



		cssmin: {
		   dist: {
		     files: {
		       '<%= config.dist %>/main.css': [
		           '<%= config.dist %>/{,*/}*.css'
		       ]
		     }
		   }
		 },

		 uglify: {
			 dist: {
				 options : {
					 sourceMap : false
				 },
				 files: [
					 {
						 expand: true,
						 dot:true,
						 cwd: '<%= config.dist %>/',
						 dest: '<%= config.dist %>/',
						 src: '{,*/}*.js'
					 }
				 ]
			 }
		 },

		 realFavicon: {
		 		favicons: {
					src: '<%= config.app %>/images/favicon.png',
		 			dest: '<%= config.dist %>/images',
		 			options: {
		 				iconsPath: 'https://aaf1a18515da0e792f78-c27fdabe952dfc357fe25ebf5c8897ee.ssl.cf5.rackcdn.com/1849/',
		 				html: [ '<%= config.app %>/views/favicon.twig' ],
		 				design: {
		 					ios: {
		 						pictureAspect: 'noChange'
		 					},
		 					desktopBrowser: {},
		 					windows: {
		 						pictureAspect: 'noChange',
		 						backgroundColor: '#da532c',
		 						onConflict: 'override'
		 					},
		 					androidChrome: {
		 						pictureAspect: 'noChange',
		 						themeColor: '#ffffff',
		 						manifest: {
		 							name: 'Nouveaux OGM',
		 							display: 'browser',
		 							orientation: 'notSet',
		 							onConflict: 'override',
		 							declared: true
		 						}
		 					},
		 					safariPinnedTab: {
		 						pictureAspect: 'silhouette',
		 						themeColor: '#5bbad5'
		 					}
		 				},
		 				settings: {
		 					scalingAlgorithm: 'Mitchell',
		 					errorOnImageTooSmall: false
		 				}
		 			}
		 		}
		 	},

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: '<%= config.app %>',
						dest: '<%= config.dist %>',
						src: [
							'images/{,*/}*'
						]
					},
					{
						expand:true,
						dot:true,
						cwd: 'node_modules/font-awesome/',
						dest: '<%= config.dist %>',
						src: [
							'fonts'
						]
					}
				]
			}
		}
  });

	grunt.registerTask('optimize', [
		'htmlmin'/* ,
		'imagemin',
		'svgmin' */
	]);


		grunt.registerTask('favicon', [
			'realFavicon'
		]);

	grunt.registerTask('build', function() {

		twigData.cdn_version = "?v=" + (new Date()).getTime();

		grunt.task.run([
			'clean:dist',
			'less',
			'cssmin',
			'concat',
			'uglify',
			'copy:dist',
			'twigRender:dist'

		]);
	});



	grunt.registerTask('dev', function() {
		grunt.task.run([
			'clean:dist',
			'less',
			'cssmin',
			'concat',
			'uglify',
			'copy:dist',
			'twigRender:dev'

		]);
	});

};
